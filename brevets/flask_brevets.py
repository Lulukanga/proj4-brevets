"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging
# logging.getLogger().setLevel(logging.INFO) # TODO remove this?

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
# TODO? Had to add this again to make it work on my side, per
#  https://stackoverflow.com/questions/60992849/attributeerror-request-object-has-no-attribute-is-xhr
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    control_dist_km = request.args.get('km', 999, type=float)  # control kms
    brevet_dist = request.args.get('brevet_dist', type=float)
    begin_date = request.args.get('begin_date')
    begin_time = request.args.get('begin_time')
    app.logger.info(f"km={control_dist_km}, brevet_dist={brevet_dist}, date={begin_date}, time={begin_time}")
    app.logger.info(f"request.args: {request.args}")
    race_start = f"{begin_date} {begin_time}"
    try:
        open_time = acp_times.open_time(control_dist_km, brevet_dist, race_start)
    except Exception as e:
        app.logger.exception(e)
        # error_msg = "Control point is too far beyond brevet distance!\nPlease try a shorter distance."
        error_msg = str(e)
        result = {"error_msg": error_msg}
    else:
        close_time = acp_times.close_time(control_dist_km, brevet_dist, race_start)
        is_final = acp_times.final_control(brevet_dist, control_dist_km)
        result = {"open": open_time, "close": close_time, "final": is_final}
    return flask.jsonify(result=result)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print(f"Opening for global access on port {CONFIG.PORT}")
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug=True)
