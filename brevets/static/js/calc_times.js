//  var SCRIPT_ROOT = {{ request.script_root|tojson|safe }} ;
//  var TIME_CALC_URL = SCRIPT_ROOT + "/_calc_times";

Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON();
});

  // Pass calctimes a <td> element containing the data for a control.
  // It extracts the distance and calls the server to get times to
  // fill in open and close times in a human-readable format.
  // (If we want to also keep the ISO-formatted times, we'll need to
  // stash them in hidden fields.)
  function calc_times(control) {
//    if (window.isDone) return;
    var km = control.find("input[name='km']").val();
    var brevet_dist = $("#brevet_dist_km").val();
    var begin_time = $("input[name='begin_time']").val();
    var begin_date = $("input[name='begin_date']").val();
    var open_time_field = control.find("input[name='open']");
    var close_time_field = control.find("input[name='close']");
    console.log(">>>" + begin_time, begin_date);

    $.getJSON(TIME_CALC_URL, { km: km, brevet_dist: brevet_dist, begin_time: begin_time, begin_date: begin_date },
      // response handler
      function(data) {
         var times = data.result;
         console.log("Got a response: " +  JSON.stringify(times));
         console.log("Response.open = " + times.open);
         open_time_field.val(moment(times.open).utc().format("ddd MM/DD HH:mm a"));
         close_time_field.val( moment(times.close).utc().format("ddd MM/DD HH:mm a"));
         if (times.final) {
             var notes_area = control.find(".notes").html("Final Control Point has been reached.").fadeIn().fadeOut(2000);
         }
         if (times.error_msg) {
            $("#errors").html(times.error_msg).fadeIn().fadeOut(3000);
            control.find("input[name='km']").val("")
            control.find("input[name='miles']").val("")
            control.find("input[name='open']").val("")
            control.find("input[name='close']").val("")
         }
       } // end of handler function
     ); // end of getJSON
    } // end of calc_times

  //Don't run this Javascript until the DOM is fully ready and loaded.
  //This means we could move our script tag anywhere in the html file and it'll work
  $(document).ready(function(){
      //right away, setting the date and time to today, right now.
      $('input[name="begin_date"]').val(new Date().toDateInputValue().slice(0,10));
      $('input[name="begin_time"]').val(new Date().toDateInputValue().slice(11,16));
      console.log(new Date().toDateInputValue().slice(11, 16))
      //this happens on return, tab out in each row of the table
      $('input[name="miles"]').change(
         function() {
             var miles = parseFloat($(this).val());
             var km = (1.609344 * miles).toFixed(1) ;
             console.log("Converted " + miles + " miles to " + km + " kilometers");
             var control_entry = $(this).parents(".control")
             var target = control_entry.find("input[name='km']");
             target.val( km );
             // Then calculate times for this entry
             calc_times(control_entry);
          });

      $('input[name="km"]').change(
         function() {
             var km = parseFloat($(this).val());
             var miles = (0.621371 * km).toFixed(1) ;
             console.log("Converted " + km + " km to " + miles + " miles");
             var control_entry = $(this).parents(".control")
             var target = control_entry.find("input[name='miles']");
             target.val( miles );
             // Then calculate times for this entry
             calc_times(control_entry);
          });

     });   // end of what we do on document ready
