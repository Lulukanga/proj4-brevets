"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math

#  Note for CIS 322:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.


def final_control(brevet_dist_km, control_dist_km):
    return (brevet_dist_km - control_dist_km) <= 0


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km < 0:
        raise Exception("Invalid Control Distance\nOnly positive distances accepted.")
    CONTROL_OPENS = {200: [math.floor(control_dist_km/34),
                           round(((control_dist_km/34) % 1) * 60)],

                     300: [math.floor((200/34) + (control_dist_km - 200) / 32),
                           round((((200 / 34) + ((control_dist_km - 200) / 32)) % 1) * 60)],

                     400: [math.floor((200/34) + (control_dist_km - 200) / 32),
                           round((((200/34) + ((control_dist_km - 200)/32)) % 1) * 60)],

                     600: [math.floor((200/34) + (200/32) + ((control_dist_km - 400) / 30)),
                           round((((200/34) + (200/32) + ((control_dist_km - 400) / 30)) % 1) * 60)],

                     1000: [math.floor((200/34) + (200/32) + (200/30) + ((control_dist_km - 600) / 28)),
                            round((((200/34) + (200/32) + (200/30) + ((control_dist_km - 600) / 28)) % 1) * 60)]}

    FINAL_OPENS = {200: [5, 53],
                   300: [9, 00],
                   400: [12, 8],
                   600: [18, 48],
                   1000: [33, 5]}
    # time = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm')
    time = arrow.get(brevet_start_time)
    final = final_control(brevet_dist_km, control_dist_km)
    WIGGLE_ROOM = brevet_dist_km * .2  # Should be 20% of total brevet distance # TODO: verify this.
    for key in CONTROL_OPENS.keys():
        if control_dist_km < key:
            hour_shift = CONTROL_OPENS[key][0]
            minute_shift = CONTROL_OPENS[key][1]
            break
        elif final and (control_dist_km <= (key + WIGGLE_ROOM)):
            hour_shift = FINAL_OPENS[key][0]
            minute_shift = FINAL_OPENS[key][1]
            break
        elif final and control_dist_km > (brevet_dist_km + WIGGLE_ROOM):
            raise Exception("Control point is too far beyond brevet distance!\nPlease try a shorter distance.")
    return time.shift(hours=+hour_shift, minutes=+minute_shift).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    # return arrow.now().isoformat()
    CONTROL_CLOSES = {0: [1, 0],  # Starting points always close one hour after open
                      200: [math.floor(control_dist_km/15), ((control_dist_km/15) % 1) * 60],
                      300: [math.floor(control_dist_km/15), ((control_dist_km/15) % 1) * 60],
                      400: [math.floor(control_dist_km/15), ((control_dist_km/15) % 1) * 60],
                      600: [math.floor(control_dist_km/15), ((control_dist_km/15) % 1) * 60],
                      1000: [math.floor((600/15) + ((control_dist_km - 600)/11.428)),
                             (((600/15) + ((control_dist_km - 600)/11.428)) % 1) * 60]}
    CONTROL_FINAL = {200: [13, 30],
                     300: [20, 0],
                     400: [27, 0],
                     600: [40, 0],
                     1000: [75, 0]}
    time = arrow.get(brevet_start_time)
    final = final_control(brevet_dist_km, control_dist_km)
    hour_shift = 0
    minute_shift = 0
    for key in CONTROL_CLOSES.keys():
        if control_dist_km <= key:
            if control_dist_km <= 60:
                hours = (control_dist_km/20) + 1
                hour_shift = math.floor(hours)
                minute_shift = (hours % 1) * 60
            else:
                hour_shift = CONTROL_CLOSES[key][0]
                minute_shift = CONTROL_CLOSES[key][1]
            break
    if final:
        for key in CONTROL_FINAL.keys():
            if control_dist_km >= key:
                hour_shift = CONTROL_FINAL[key][0]
                minute_shift = CONTROL_FINAL[key][1]
    return time.shift(hours=+hour_shift, minutes=+minute_shift).isoformat()


##########################################################################



# print("400km ACP Brevet:")
# open0 = open_time(0, 400, '2013-01-01 12:00')
# close0 = close_time(0, 400, '2013-01-01 12:00')
# print("start open:", open0)
# print("start close:", close0, "\n")
#

# print(open_time(60, 200, '2013-01-01T12:00-00:00'))
# print(open_time(0, 200, arrow.now().isoformat()))
#
# print(close_time(0, 200, arrow.now().isoformat()))
#
# print(close_time(0, 200, arrow.now().isoformat()))
#
# print(arrow.now().isoformat())

# open150 = open_time(150, 400, '2013-01-01 12:00')
# close150 = close_time(150, 400, '2013-01-01 12:00')
# print("150 open:", open150)
# print("150 close:", close150, "\n")
#
# open300 = open_time(300, 400, '2013-01-01 12:00')
# close300 = close_time(300, 400, '2013-01-01 12:00')
# print("300 open:", open300)
# print("300 close:", close300, "\n")
#
# open400 = open_time(400, 400, '2013-01-01 12:00')
# close400 = close_time(400, 400, '2013-01-01 12:00')
# print("400 open final: ", open400)
# print("400 close final:", close400)
# print("\n\n")
# # ##########################################################################
# print("Example 1")
# print("200km ACP Brevet:")
# open0 = open_time(0, 200, '2013-01-01 12:00')
# close0 = close_time(0, 200,'2013-01-01 12:00')
# print("start open:", open0)
# print("start close:", close0, "\n")
#
# open60 = open_time(60, 200, '2013-01-01 12:00')
# close60 = close_time(60, 200, '2013-01-01 12:00')
# print("60 open:", open60)
# print("60 close:", close60, "\n")
#
# open120 = open_time(120, 200, '2013-01-01 12:00')
# close120 = close_time(120, 200, '2013-01-01 12:00')
# print("120 open:", open120)
# print("120 close:", close120, "\n")
#
# open175 = open_time(175, 200, '2013-01-01 12:00')
# close175 = close_time(175, 200, '2013-01-01 12:00')
# print("175 open:", open175)
# print("175 close:", close175, "\n")
#
# open200 = open_time(200, 200, '2013-01-01 12:00')
# close200 = close_time(200, 200, '2013-01-01 12:00')
# print("200 open final: ", open200)
# print('200, close final:', close200, "\n")
#
# open205 = open_time(205, 200, '2013-01-01 12:00')
# close205 = close_time(205, 200, '2013-01-01 12:00')
# print("205 open final: ", open205)
# print("205 close final:", close205, "\n")
#
# open240 = open_time(240, 200, '2013-01-01 12:00')
# close240 = close_time(240, 200, '2013-01-01 12:00')
# print("240 open final: ", open240)
# print("240 close final:", close240, "\n")

# open241 = open_time(241, 200, '2020-01-01 12:00')
# print("241 open: ", open241)
# print("\n")
# ##########################################################################
# print("Example2")
# print("600km ACP Brevet ")
# open0 = open_time(0, 600, '2013-01-01 12:00')
# close0 = close_time(0, 600, '2013-01-01 12:00')
# print("start open:", open0)
# print("start close:", close0, "\n")
#
# open50 = open_time(50, 600, '2013-01-01 12:00')
# close50 = close_time(50, 600, '2013-01-01 12:00')
# print(open50)
# print(close50, "\n")
#
# open100 = open_time(100, 600, '2013-01-01 12:00')
# close100 = close_time(100, 600, '2013-01-01 12:00')
# print(open100)
# print(close100, "\n")
#
# open150 = open_time(150, 600, '2013-01-01 12:00')
# close150 = close_time(150, 600, '2013-01-01 12:00')
# print(open150)
# print(close150, "\n")
#
# open200 = open_time(200, 600, '2013-01-01 12:00')
# close200 = close_time(200, 600, '2013-01-01 12:00')
# print(open200)
# print(close200, "\n")
#
# open250 = open_time(250, 600, '2013-01-01 12:00')
# close250 = close_time(250, 600, '2013-01-01 12:00')
# print(open250)
# print(close250, "\n")
#
# open300 = open_time(300, 600, '2013-01-01 12:00')
# close300 = close_time(300, 600, '2013-01-01 12:00')
# print(open300)
# print(close300, "\n")
#
# open350 = open_time(350, 600, '2013-01-01 12:00')
# close350 = close_time(350, 600, '2013-01-01 12:00')
# print(open350)
# print(close350, "\n")
#
# open400 = open_time(400, 600, '2013-01-01 12:00')
# close400 = close_time(400, 600, '2013-01-01 12:00')
# print(open400)
# print(close400, "\n")
#
# open450 = open_time(450, 600, '2013-01-01 12:00')
# close450 = close_time(450, 600, '2013-01-01 12:00')
# print(open450)
# print(close450, "\n")
#
# open500 = open_time(500, 600, '2013-01-01 12:00')
# close500 = close_time(500, 600, '2013-01-01 12:00')
# print(open500)
# print(close500, "\n")
#
# open550 = open_time(550, 600, '2013-01-01 12:00')
# close550 = close_time(550, 600, '2013-01-01 12:00')
# print(open550)
# print(close550, "\n")
#
# open600 = open_time(609, 600, '2013-01-01 12:00')
# close600 = close_time(609, 600, '2013-01-01 12:00')
# print(open600)
# print(close600)
#
# print("\n")
# ######################################################
# print("Example 3")
# print("1000km ACP Brevet")
# open0 = open_time(0, 1000, '2013-01-01 12:00')
# print("start open:", open0)
# open890 = open_time(890, 1000, '2013-01-01 12:00')
# print(open890)
# open1000 = open_time(1000, 1000, '2013-01-01 12:00', True)
# print(open1000)
# print("\n\n")
# ######################################################
# print("Brevet2: 1000km")
# open0 = open_time(0, 1000, '2013-01-01 12:00')
# print("start open:", open0)
# open100 = open_time(100, 1000, '2013-01-01 12:00')
# print(open100)
# open200 = open_time(200, 1000, '2013-01-01 12:00')
# print(open200)
# open300 = open_time(300, 1000, '2013-01-01 12:00')
# print(open300)
# open400 = open_time(400, 1000, '2013-01-01 12:00')
# print(open400)
# open500 = open_time(500, 1000, '2013-01-01 12:00')
# print(open500)
# open600 = open_time(600, 1000, '2013-01-01 12:00')
# print(open600)
# open700 = open_time(700, 1000, '2013-01-01 12:00')
# print(open700)
# open800 = open_time(800, 1000, '2013-01-01 12:00')
# print(open800)
# open900 = open_time(900, 1000, '2013-01-01 12:00')
# print(open900)
# open1000 = open_time(1000, 1000, '2013-01-01 12:00', True)
# print(open1000)
# open1200 = open_time(1200, 1000, '2013-01-01 12:00', True)
# print(open1200)
# print("\n\n")
# ##########################################################################
# print("Brevet3: 1000km, at 99's")
# open0 = open_time(0, 1000, '2013-01-01 12:00')
# print("start open:", open0)
# open99 = open_time(99, 1000, '2013-01-01 12:00')
# print(open99)
# open199 = open_time(199, 1000, '2013-01-01 12:00')
# print(open199)
# open299 = open_time(299, 1000, '2013-01-01 12:00')
# print(open299)
# open399 = open_time(399, 1000, '2013-01-01 12:00')
# print(open399)
# open499 = open_time(499, 1000, '2013-01-01 12:00')
# print(open499)
# open599 = open_time(599, 1000, '2013-01-01 12:00')
# print(open599)
# open699 = open_time(699, 1000, '2013-01-01 12:00')
# print(open699)
# open799 = open_time(799, 1000, '2013-01-01 12:00')
# print(open799)
# open899 = open_time(899, 1000, '2013-01-01 12:00')
# print(open899)
# open1000 = open_time(1000, 1000, '2013-01-01 12:00', True)
# print(open1000)
# print("\n\n")
# ##########################################################################
# print("Brevet4: 1000km, at 50's")
# open0 = open_time(0, 1000, '2013-01-01 12:00')
# print("start open:", open0)
# open50 = open_time(50, 1000, '2013-01-01 12:00')
# print(open50)
# open100 = open_time(100, 1000, '2013-01-01 12:00')
# print(open100)
# open150 = open_time(150, 1000, '2013-01-01 12:00')
# print(open150)
# open200 = open_time(200, 1000, '2013-01-01 12:00')
# print(open200)
# open250 = open_time(250, 1000, '2013-01-01 12:00')
# print(open250)
# open300 = open_time(300, 1000, '2013-01-01 12:00')
# print(open300)
# open350 = open_time(350, 1000, '2013-01-01 12:00')
# print(open350)
# open400 = open_time(400, 1000, '2013-01-01 12:00')
# print(open400)
# open450 = open_time(450, 1000, '2013-01-01 12:00')
# print(open450)
# open500 = open_time(500, 1000, '2013-01-01 12:00')
# print(open500)
# open550 = open_time(550, 1000, '2013-01-01 12:00')
# print(open550)
# open600 = open_time(600, 1000, '2013-01-01 12:00')
# print(open600)
# open650 = open_time(650, 1000, '2013-01-01 12:00')
# print(open650)
# open700 = open_time(700, 1000, '2013-01-01 12:00')
# print(open700)
# open750 = open_time(750, 1000, '2013-01-01 12:00')
# print(open750)
# open800 = open_time(800, 1000, '2013-01-01 12:00')
# print(open800)
# open850 = open_time(850, 1000, '2013-01-01 12:00')
# print(open850)
# open900 = open_time(900, 1000, '2013-01-01 12:00')
# print(open900)
# open950 = open_time(950, 1000, '2013-01-01 12:00')
# print(open950)
# open1000 = open_time(1000, 1000, '2013-01-01 12:00', True)
# print(open1000)
# print("\n\n")