import nose
from nose.tools import *
from acp_times import open_time, close_time
import arrow

"""your test cases should be chosen to distinguish between an 
implementation that correctly interprets the ACP rules and one that does not."""


def test_200km_brevet_opentimes_example1():
    """
    Checking Example 1 from https://rusa.org/pages/acp-brevet-control-times-calculator
    Dates should come in from our page, and be sent to the open_times()
    function in a 'YYYY-MM-DD HH:mm' format.
    open_time(60, 200, arrow.now().isoformat()) is a 1 hr, 46 min open time:
    open_time(120, 200, arrow.now().isoformat) => 3 hr, 32 min
    open_time(175, 200, arrow.now().isoformat) => 5 hrs, 9 min
    open_time(205, 200, arrow.now().isoformat) => 5 hrs, 53 min
    open_time(240, 200, arrow.now().isoformat) => 5 hrs, 53 min
    open_time(240, 200, arrow.now().isoformat) => 5 hrs, 53 min
    open_time(241, 200, arrow.now().isoformat) => 6 hrs, 15 min
    """
    assert open_time(0, 200, '2021-11-21T15:30:30.159506-08:00') == '2021-11-21T15:30:30.159506-08:00'

    assert open_time(60, 200, '2013-01-01T12:00+00:00') == '2013-01-01T13:46:00+00:00'

    assert open_time(120, 200, '2013-01-01T12:00+00:00') == '2013-01-01T15:32:00+00:00'

    assert open_time(175, 200, '2013-01-01T12:00+00:00') == '2013-01-01T17:09:00+00:00'

    assert open_time(205, 200, '2013-01-01T12:00+00:00') == '2013-01-01T17:53:00+00:00'


def test_600km_brevet_opentimes_example2():
    """
    Checking open_times for Example 2 from
    https://rusa.org/pages/acp-brevet-control-times-calculator
    """
    assert open_time(0, 600, '2013-01-01 12:00') == arrow.get(
        "2013/01/01 12:00", "YYYY/MM/DD HH:mm").isoformat()

    assert open_time(50, 600, '2013-01-01 12:00') == arrow.get(
        "2013/01/01 13:28", "YYYY/MM/DD HH:mm").isoformat()

    assert open_time(100, 600, '2013-01-01 12:00') == arrow.get(
        "2013/01/01 14:56", "YYYY/MM/DD HH:mm").isoformat()

    assert open_time(150, 600, '2013-01-01 12:00') == arrow.get(
        "2013/01/01 16:25", "YYYY/MM/DD HH:mm").isoformat()

    assert open_time(200, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 17:53", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(250, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 19:27", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(300, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 21:00", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(350, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 22:34", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(400, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 00:08", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(450, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 01:48", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(500, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 03:28", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(550, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 05:08", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(609, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 06:48", "ddd YYYY/MM/DD HH:mm").isoformat()


def test_1000km_brevet_opentimes_example3():
    """
    Checking Example 3 from https://rusa.org/pages/acp-brevet-control-times-calculator
    """
    assert open_time(0, 1000, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 12:00", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(890, 1000, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 17:09", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(1000, 1000, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 21:05", "ddd YYYY/MM/DD HH:mm").isoformat()


def test_200km_finalbrevet_wiggleroom_opentimes():
    """
    Wiggle room is defined to be some certain percentage of the total brevet distance.
    If any final control point goes is less than that wiggle room, the opening time
    should be calculated on the lower threshold opening time. This helps to
    ensure that no one will get to the final control point too early and have a
    lonely celebration.
    For example, the final control point, at 205 km, in a "200km" brevet should have
    an opening time 5 hrs, 53 minutes after the initial start (distance/max_speed),
    rather than 6 hrs, 24 minutes.
    """
    assert open_time(200, 200, '2020-01-01 12:00') == arrow.get(
        "Wed 2020/01/01 17:53", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(205, 200, '2020-01-01 12:00') == arrow.get(
        "Wed 2020/01/01 17:53", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(240, 200, '2020-01-01 12:00') == arrow.get(
        "Wed 2020/01/01 17:53", "ddd YYYY/MM/DD HH:mm").isoformat()


def test_1000km_99_opentimes():
    """
    Edge cases for open_times.
    """
    # Brevets with control points just under the demarcation points
    assert open_time(0, 1000, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 12:00", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(99, 1000, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 14:55", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(199, 1000, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 17:51", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(899, 1000, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 17:29", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(1000, 1000, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 21:05", "ddd YYYY/MM/DD HH:mm").isoformat()

    # Brevets with control points just over the demarcation points
    assert open_time(0, 1000, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 12:00", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(101, 1000, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 14:58", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(201, 1000, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 17:55", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(301, 1000, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 21:02", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(401, 1000, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 00:10", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(601, 1000, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 06:50", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(1001, 1000, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 21:05", "ddd YYYY/MM/DD HH:mm").isoformat()


def test_example1_close_times200km():
    """
    Checking close times for Example 1 from
    https://rusa.org/pages/acp-brevet-control-times-calculator
    """
    # all starting points have a close time of 1 hour
    assert close_time(0, 200, '2013-01-01 12:00') == arrow.get(
        "2013/01/01 13:00", "YYYY/MM/DD HH:mm").isoformat()

    # normal control points
    assert close_time(60, 200, '2013-01-01 12:00') == arrow.get(
        "2013/01/01 16:00", "YYYY/MM/DD HH:mm").isoformat()

    assert close_time(120, 200, '2013-01-01 12:00') == arrow.get(
        "2013/01/01 20:00", "YYYY/MM/DD HH:mm").isoformat()

    assert close_time(175, 200, '2013-01-01 12:00') == arrow.get(
        "2013/01/01 23:40", "YYYY/MM/DD HH:mm").isoformat()

    # all ending control points up to 20% of total brevet distance should
    #  use the total brevet distance for close times calculations, regardless
    #  of how much further they go.
    assert close_time(200, 200, '2013-01-01 12:00') == arrow.get(
        "2013/01/02 01:30", "YYYY/MM/DD HH:mm").isoformat()

    assert close_time(205, 200, '2013-01-01 12:00') == arrow.get(
        "2013/01/02 01:30", "YYYY/MM/DD HH:mm").isoformat()

    assert close_time(240, 200, '2013-01-01 12:00') == arrow.get(
        "2013/01/02 01:30", "YYYY/MM/DD HH:mm").isoformat()


def test_example2_full_brevet_w_relaxed_controls():
    """
    As of March 2018, relaxed control close times are allowed outside of France.
    Testing relaxed controls per example 2 from
    https://rusa.org/pages/acp-brevet-control-times-calculator
    """
    assert open_time(0, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 12:00", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert close_time(0, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 13:00", "ddd YYYY/MM/DD HH:mm").isoformat()

    # The first control is a "relaxed control", per March 2018 updates
    assert open_time(50, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 13:28", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert close_time(50, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 15:30", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(100, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 14:56", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert close_time(100, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 18:40", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(150, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 16:25", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert close_time(150, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 22:00", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(200, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 17:53", "ddd YYYY/MM/DD HH:mm").isoformat()
    assert close_time(200, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 01:20", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(250, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 19:27", "ddd YYYY/MM/DD HH:mm").isoformat()
    assert close_time(250, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 04:40", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(300, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 21:00", "ddd YYYY/MM/DD HH:mm").isoformat()
    assert close_time(300, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 08:00", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(350, 600, '2013-01-01 12:00') == arrow.get(
        "Tue 2013/01/01 22:34", "ddd YYYY/MM/DD HH:mm").isoformat()
    assert close_time(350, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 11:20", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(400, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 00:08", "ddd YYYY/MM/DD HH:mm").isoformat()
    assert close_time(400, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 14:40", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(450, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 01:48", "ddd YYYY/MM/DD HH:mm").isoformat()
    assert close_time(450, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 18:00", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(500, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 03:28", "ddd YYYY/MM/DD HH:mm").isoformat()
    assert close_time(500, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 21:20", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(550, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 05:08", "ddd YYYY/MM/DD HH:mm").isoformat()
    assert close_time(550, 600, '2013-01-01 12:00') == arrow.get(
        "Thu 2013/01/03 00:40", "ddd YYYY/MM/DD HH:mm").isoformat()

    # The final control point closes at the same time it would if it were 600 km.
    # The fact that we actually go to 609 km is irrelevant.
    assert open_time(609, 600, '2013-01-01 12:00') == arrow.get(
        "Wed 2013/01/02 06:48", "ddd YYYY/MM/DD HH:mm").isoformat()
    assert close_time(609, 600, '2013-01-01 12:00') == arrow.get(
        "Thu 2013/01/03 04:00", "ddd YYYY/MM/DD HH:mm").isoformat()


def test_relaxed_controls():
    assert open_time(50, 200, '2021-02-02 12:00') == arrow.get(
        "Tue 2021/02/02 13:28", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(50, 200, '2021-03-02 12:00') == arrow.get(
        "Tue 2021/03/02 13:28", "ddd YYYY/MM/DD HH:mm").isoformat()

    assert open_time(30, 200, '2021-03-02 09:00') == arrow.get(
        "Tue 2021/03/02 09:53", "ddd YYYY/MM/DD HH:mm").isoformat()


def test_bad_brevet_times():
    """If the attempted final control distance is further than 20% we should throw an error."""
    # For a 200km brevet, 241 km is more than 20% beyond the total brevet distance.
    with assert_raises(Exception):
        open_time(241, 200, '2020-01-01 12:00')

    # For a 300 km brevet, 360 km is the max distance for a final control point.
    with assert_raises(Exception):
        open_time(360.1, 300, '2020-01-01 12:00')

    # For a 400 km brevet, 480 km is the max distance for a final control point.
    with assert_raises(Exception):
        open_time(480.00001, 400, '2020-01-01 12:00')

    # For a 600 km brevet, 720 km is the max distance for a final control point.
    with assert_raises(Exception):
        open_time(721, 600, '2020-01-01 12:00')

    # For a 1000 km brevet, 1200 km is the max distance for a final control point.
    with assert_raises(Exception):
        open_time(1200.00001, 1000, '2020-01-01 12:00')


@raises(Exception)
def test_bad_dates():
    """There are some dates that just ain't right."""
    # I would call this month Wintruary. It would be horrible.
    open_time(50, 200, '2020-13-01 12:00')

    # Instead of Groundhogs Day, could we have Honeybadger Day? Either of these days would work for me.
    open_time(50, 200, '2021-02-00 12:00')
    open_time(50, 200, '2021-02-30 12:00')

    # This wasn't a leap year, thankfully.
    open_time(50, 200, '2021-02-29 12:00')

    # We've found the singularity. It's here and it's not so bad.
    open_time(50, 200, '0000-00-00 00:00')


@raises(Exception)
def test_bad_controls():
    """You can't go negative distances y'all."""
    open_time(-1, 200, '2021-11-01 12:00')
    open_time(-1.5, 200, '2021-11-01 12:00')
    open_time(-0.00000001, 200, '2021-11-01 12:00')

    open_time(-1, 300, '2021-11-01 12:00')
    open_time(-1, 400, '2021-11-01 12:00')
    open_time(-1, 1000, '2021-11-01 12:00')


